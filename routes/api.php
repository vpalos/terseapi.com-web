<?php

use App\Http\Controllers\NetworkController;
use App\Http\Controllers\MetaController;
use App\Http\Controllers\ReferenceController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "api" middleware group. Now create something great!
|
*/

/**
 * Reference endpoint.
 * This needs to be served from the `api` group to benefit from the CORS rules.
 */
Route::get('/reference.json', [ReferenceController::class, 'getJson'])->name('api.reference.json');

/**
 * Public endpoints.
 */
Route::get('/meta', [MetaController::class, "getMeta"]);
Route::get('/identify', [NetworkController::class, "getIdentify"]);
