<?php

use App\Http\Controllers\ReferenceController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Reference endpoints.
 */
Route::view('/reference', 'reference.index')->name('api.reference');

/**
 * Root default.
 */
Route::redirect('/', '/reference');
