<?php

namespace App\Services;

class TerseService
{
    public function meta()
    {
        return [
            'name' => config('app.name'),
            'version' => config('app.version'),
            'urls' => [
                'base' => url('/'),
                'reference' => route('api.reference'),
                'reference.json' => route('api.reference.json'),
            ],
            'time' => [
                'iso' => now(),
                'stamp' => time(),
            ],
        ];
    }
}
