<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class Terse extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'terse';
    }
}
