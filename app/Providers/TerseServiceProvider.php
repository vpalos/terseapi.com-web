<?php

namespace App\Providers;

use App\Services\TerseService;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class TerseServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('terse', fn () => new TerseService);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
