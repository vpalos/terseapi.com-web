<?php

namespace App\Http\Controllers;

use App\Facades\Terse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Jenssegers\Agent\Facades\Agent;
use Torann\GeoIP\Facades\GeoIP;

class NetworkController extends Controller
{
    public function getIdentify(Request $request)
    {
        return new JsonResponse([
            'ip' => $request->ip(),
            'agent' => [
                'text' => $request->server('HTTP_USER_AGENT'),
                'device' => [
                    'name' => Agent::device(),
                    'type' => Agent::deviceType(),
                ],
                'browser' => [
                    'name' => $browser = Agent::browser(),
                    'version' => Agent::version($browser),
                ],
                'os' => [
                    'name' => $os = Agent::platform(),
                    'version' => Agent::version($os),
                ],
                'robot' => Agent::robot(),
            ],
            'location' => GeoIP::getLocation()->toArray(),
            'meta' => Terse::meta()
        ], 200, [], JSON_PRETTY_PRINT);
    }
}
