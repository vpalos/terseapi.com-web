<?php

namespace App\Http\Controllers;

use App\Facades\Terse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class MetaController extends Controller
{
    public function getMeta(Request $request)
    {
        return new JsonResponse(Terse::meta(), 200, [], JSON_PRETTY_PRINT);
    }
}
