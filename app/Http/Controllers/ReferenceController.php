<?php

namespace App\Http\Controllers;

use Symfony\Component\Yaml\Yaml;

class ReferenceController extends Controller
{
    public function getJson()
    {
        $reference = Yaml::parseFile(resource_path('api/reference.yaml'));

        $reference['servers'] = [
            ['url' => url(config('app.url'))]
        ];

        return response()->json($reference);
    }
}
